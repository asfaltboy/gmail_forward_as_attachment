InboxSDK.load('1.0', 'sdk_FwdAsAttachment_fa45310a87').then(function(sdk){

  sdk.Toolbars.registerToolbarButtonForList({
	  title: "Forward as Attachment",
	  section: sdk.Toolbars.SectionNames.INBOX_STATE,
	  onClick: function(event) {
		sdk.ButterBar.showMessage({
			text: "Getting selected thread messages content",
			time: 3000
		});

		// map thread ID to subject
		var threadIdMap = new Map(event.selectedThreadRowViews.map(function(tr) {
			return [tr.getThreadID(), tr.getSubject()];
		}));
		var request = {threads: Array.from(threadIdMap.keys())};

		// offload fetching work to background page
		chrome.runtime.sendMessage(request, function(messages) {
			// for each fetched result, create a file blob and a link to it
			var files = [];
			var items = messages.map(function(result, i) {
				var subject = threadIdMap.get(result.threadId);
				var filename = subject + '.eml';
				var file = new File([result.content], filename, { type: 'message/rfc822'});
				var fileUrl = window.URL.createObjectURL(file);
				files.push(file);
				return '<li><a href="' + fileUrl + '" download="' + filename + '">' + subject + '</a></li>';
			});

			// display links to download attachments in a modal
			sdk.Widgets.showModalView({
				title: "Attachments ready",
				el: '<ul>' + items.join('') + '</ul>',
				buttons: [{
					text: 'Attach to new email',
					onClick: function() {
						sdk.Compose.registerComposeViewHandler(function(composeView) {
							// add our attachments when compose view is created
							if (files.length) {
								composeView.attachFiles(files);
								files = [];
							}
						});

						// open the compose view (will call the above handler)
						sdk.Compose.openNewComposeView();
					}
				}]
			});
		});
	  }
  });

});
