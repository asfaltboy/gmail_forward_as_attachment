//oauth2 auth
chrome.identity.getAuthToken(
	{'interactive': true},
	function(){
		//load Google's javascript client libraries
		window.gapi_onload = authorize;
		loadScript('https://apis.google.com/js/client.js');
	}
);

function loadScript(url){
	var request = new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(request.readyState !== 4) {
			return;
		}

		if(request.status !== 200){
			return;
		}

		eval(request.responseText);
	};

	request.open('GET', url);
	request.send();
}

function authorize(){
	chrome.identity.getAuthToken({ 'interactive': true }, function(token) {
		gapi.auth.setToken({access_token: token});
		gapi.client.load('gmail', 'v1', gmailAPILoaded);
	});
}

function gmailAPILoaded(){
	chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
		console.log('Sending messages as attachements from threads: ' + request.threads);
		window.getMessagesRawBatch(request.threads, function(results) {
			var response = Object.values(results).map(function(res) {
				return {
					content: window.getThreadRaw(res),
					threadId: window.getThreadThreadId(res)
				}
			});
			sendResponse(response);
		})
		return true;
	});
}


//takes in an array of thread IDs
function getMessagesRawBatch(threadIds, callback){
	var batch = new gapi.client.newBatch();
	threadIds.forEach(function(threadId) {
		batch.add(gapi.client.gmail.users.messages.get({
			userId: 'me',
			id: threadId,
			format: "RAW"
		}));
	});

	batch.execute(callback);
}

function getThreadRaw(threadDetails) {
	var raw_body = threadDetails.result.raw;
	return B64.decode(raw_body);
}

function getThreadThreadId(threadDetails) {
	return threadDetails.result.threadId;
}
