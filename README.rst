Introduction
============
This is the source code for the extension: https://chrome.google.com/webstore/detail/gmail-forward-as-attachme/glhbiolbojnegkfaollkgldiockgjlom


Inspired by multi-forward-for-gmail_, this extension aims to add the missing "download email" or "send email as attachment" functionality, commonly found in desktop email clients.

It does this by first finding the IDs of the selected threads in the mailbox, then fetching the RAW messages in given threads, saving them as in-memory File objects and finally providing download links for the files with ``<thread subject>.eml`` filenames. Additionally, an "Attach to new email" button opens a new compose window and attaches the files of the threads selected.

The UI interactions are provided by InboxSDK_, and the rest is done with Gmail-API_.

Credits
~~~~~~~

Icon is a mashup of three icons from www.flaticon.com; gmail icon made by Freepik, attachment and forward icons made by Madebyoliver.

Todo
~~~~

- Render .eml files comfortably inline

  Related Stack overflow questions:

  - http://webapps.stackexchange.com/questions/31689/how-to-view-an-attached-email-in-gmail
  - http://webapps.stackexchange.com/questions/13246/sending-a-new-mail-with-another-email-as-an-attachment-in-gmail?rq=1

References
~~~~~~~~~~

- http://developer.streak.com/2014/10/how-to-use-gmail-api-in-chrome-extension.html
- https://gist.github.com/omarstreak/7908035c91927abfef59
- https://developers.google.com/gmail/api/v1/reference/users/messages/get
- https://www.w3.org/Protocols/rfc822/
- https://developer.mozilla.org/en-US/docs/Web/API/File

.. _multi-forward-for-gmail: https://chrome.google.com/webstore/detail/multi-forward-for-gmail/jjmdplljmniahpamcmabdnahmjdlikpm
.. _InboxSDK: https://www.inboxsdk.com/
.. _Gmail-API: https://developers.google.com/gmail/api/